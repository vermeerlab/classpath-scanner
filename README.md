[FastClasspathScanner](https://github.com/lukehutch/fast-classpath-scanner)を拡張した機能を提供するクラス群です
===

## Requirement
事前にローカルリポジトリに `parentpom` プロジェクトを作成してください。

`git clone https://bitbucket.org/vermeerlab/parentpom.git`

## Usage
Maven Repository

https://github.com/vermeerlab/maven/tree/mvn-repo/org/vermeerlab/vermeerlab-classpath-scanner

## Licence
Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author
[_vermeer_](https://twitter.com/_vermeer_)
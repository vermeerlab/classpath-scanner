/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.scanner;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

/**
 * Propertiesファイルを参照する機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
public class PropertyReader {

    List<Properties> propertiesList;

    PropertyReader() {
    }

    PropertyReader(List<Properties> propertiesList) {
        this.propertiesList = propertiesList;
    }

    /**
     * インスタンスを構築します.
     *
     * @return 構築したインスタンス
     */
    public static PropertyReader of() {
        List<Properties> _propertiesList = new ArrayList<>();

        new FastClasspathScanner("-jar:*.jar")
                .addClassLoader(PropertyReader.class.getClassLoader())
                .matchFilenameExtension("properties",
                                        (String relativePath, InputStream inputStream, long lengthBytes) -> {
                                            try {
                                                Properties _properties = new Properties();
                                                _properties.load(inputStream);
                                                _propertiesList.add(_properties);
                                            } finally {
                                                inputStream.close();
                                            }
                                        }).scan();
        return new PropertyReader(_propertiesList);
    }

    /**
     * Classpath上の全Propertiesを対象にkeyが一致したvalueを返却します.
     * <P>
     * 対象キーが複数存在した場合は 最初に取得した値を返却するため正確性の保証はされません.
     *
     * @param key 検索対象のpropertiesキー
     * @return keyが一致したpropertiesのvalue
     */
    public Optional<String> getValue(String key) {
        return this.propertiesList.stream()
                .map(properties -> properties.getProperty(key))
                .filter(value -> value != null)
                .findFirst();
    }
}

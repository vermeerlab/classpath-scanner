/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.scanner;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * 指定のインターフェースを実装した具象クラス情報を取得します.
 *
 * @author Yamashita,Takahiro
 */
public class ImplementClasses {

    private Class<?> implementedInterface;

    ImplementClasses() {
    }

    ImplementClasses(Class<?> implementedInterface) {
        this.implementedInterface = implementedInterface;
    }

    /**
     * インスタンスを構築します.
     *
     * @param implementedInterface 取得対象の型（インターフェース）
     * @return 構築したインスタンス
     */
    public static ImplementClasses of(Class<?> implementedInterface) {
        return new ImplementClasses(implementedInterface);
    }

    /**
     * インターフェースを実装した具象クラスのパスを返却します.
     *
     * @return インターフェースの具象クラスのパスリスト
     */
    public Set<String> get() {
        final Set<String> results = new ConcurrentSkipListSet<>();
        new FastClasspathScanner().addClassLoader(implementedInterface.getClassLoader())
                .matchClassesImplementing(implementedInterface, (clazz) -> {
                                      String clazzName = clazz.getName();
                                      if (clazzName.contains("$")) {
                                          return;
                                      }
                                      if (Modifier.isAbstract(clazz.getModifiers())) {
                                          return;
                                      }
                                      results.add(clazzName);
                                  })
                .scan();
        return results;
    }
}

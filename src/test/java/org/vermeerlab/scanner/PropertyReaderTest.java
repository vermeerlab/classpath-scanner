/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.scanner;

import java.util.Optional;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class PropertyReaderTest {

    @Test
    public void existsKey() {
        String value = PropertyReader.of().getValue("test1key").get();
        Assert.assertThat(value, is("test1value"));
    }

    @Test
    public void notExistsKey() {
        Optional<String> notExists = PropertyReader.of().getValue("test3key");
        Assert.assertThat(notExists.isPresent(), is(false));
    }

    @Test
    public void duplicateKey() {
        Optional<String> value = PropertyReader.of().getValue("duplicateKey");
        Assert.assertThat(value.isPresent(), is(true));
    }

    @Test
    public void コンストラクタ_カバレッジ対応() {
        PropertyReader propertyReader = new PropertyReader();
    }
}
